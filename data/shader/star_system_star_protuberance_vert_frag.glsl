//###vert
#version 330
in vec4 vert;

uniform mat4 tr;

void main()
{
    gl_Position = tr * vert;
}

//###frag
#version 330
uniform vec3 color;

out vec4 out_color;

void main()
{
    out_color = vec4( color, 1 );
}
