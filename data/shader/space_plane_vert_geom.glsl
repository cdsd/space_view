//###vert
#version 330
in vec3 vert;
in float mag;
in vec3 color;

out vec3 e_vert;
out float e_mag;
out vec3 e_color;

void main()
{
    e_vert = vert;
    e_mag = mag;
    e_color = color;
}
//###geom
#version 330
in vec3 e_vert[];
in float e_mag[];
in vec3 e_color[];

uniform mat4 camera;
uniform mat4 projection;

layout(points) in;
layout(triangle_strip,max_vertices=4) out;

out vec2 local_pos;
out vec3 ex_color;
out float ex_size;

const float PI = 3.14;

void main()
{
    float size = e_mag[0] / 6.0;
    vec4 tr_pos = camera * vec4( e_vert[0], 0 );

    vec4 ex = vec4( cross( tr_pos.xyz, vec3( 0, 1, 0 ) ), 1 );
    ex.xyz = normalize( ex.xyz );
    vec4 ey = vec4( cross( ex.xyz, tr_pos.xyz ), 1 );
    ey.xyz = normalize( ey.xyz );

    gl_Position = projection * ( ( -ex - ey ) * size + tr_pos );
    local_pos = vec2( 0, 0 );
    ex_color = e_color[0];
    ex_size = size;
    EmitVertex();

    gl_Position = projection * ( ( ex - ey ) * size + tr_pos ); 
    local_pos = vec2( 1, 0 );
    ex_color = e_color[0];
    ex_size = size;
    EmitVertex();

    gl_Position = projection * ( ( -ex + ey ) * size + tr_pos ); 
    local_pos = vec2( 0, 1 );
    ex_color = e_color[0];
    ex_size = size;
    EmitVertex();

    gl_Position = projection * ( ( ex + ey ) * size + tr_pos ); 
    local_pos = vec2( 1, 1 );
    ex_color = e_color[0];
    ex_size = size;
    EmitVertex();

    EndPrimitive();
}
