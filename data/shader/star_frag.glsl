//###frag
#version 330
in vec2 local_pos;
in float ex_size;
in vec3 ex_color;

out vec4 color;

float sqr( float v ){ return v * v; }

void main()//TODO better drawing
{ 
    if( ex_size < 0.2 )
    {
        color.w = 0;
        return;
    }
    vec2 pos = local_pos - vec2( 0.5 );
    float dist = length( pos );
    float max_dist = vec2( 0.5 );
    color.xyz = ex_color;
    color.w = 1 - dist / max_dist - 0.3;
}
