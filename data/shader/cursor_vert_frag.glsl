//###vert
#version 330
in vec2 vert;

uniform vec2 win_size;

void main()
{
    vec2 tr_vert = vert / win_size * 2 - 1;
    gl_Position = vec4( tr_vert.x, -tr_vert.y, 0, 1);
}

//###frag
#version 330
uniform vec4 color;

out vec4 out_color;

void main()
{
    out_color = color;
}
