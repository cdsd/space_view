//###frag
#version 330
in vec2 local_pos;

out vec4 color;

float sqr( float v ){ return v * v; }

void main()
{
    vec2 pos = local_pos * 2 - vec2(1);
    float len2 = sqr( pos.x ) + sqr( pos.y );
    float minlen = 0.4;
    float maxlen = 0.6;
    if( len2 > minlen && len2 < maxlen )
        color = vec4( 0.8, 0.8, 0.7, 0.7 );
    else color = vec4( 0 );
}
