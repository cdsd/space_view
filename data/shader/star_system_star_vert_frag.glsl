//###vert
#version 330
in vec4 vert;
in vec2 in_uv;

uniform mat4 tr;

out vec2 uv;

void main()
{
    gl_Position = tr * vert;
    uv = in_uv;
}
//###frag
#version 330
in vec2 uv;

uniform vec3 color;

out vec4 out_color;

uniform float anim_state;
uniform int current_perlin_const;
uniform int next_perlin_const;

int iceil( float v )
{ return int( v ); }

float noise( int x, int y, int z )
{
    int n = x + y * 57 + z * 138;
    //n = int( pow( float( n << 13 ), float( n ) ) );//TODO check for better noise
    return ( 1.0 - ( (n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

vec3 bi_mix( vec3[4] vv, vec2 a )
{
    vec3 vv01 = mix( vv[0], vv[1], a.x );
    vec3 vv23 = mix( vv[2], vv[3], a.x );
    return mix( vv01, vv23, a.y );
}

vec3[4] generateNoiseRect( vec2 cuv, vec2 nuv, int z )
{
    vec3[4] res;
    res[0] = vec3( noise( iceil( cuv.x ), iceil( cuv.y ), z ) );
    res[1] = vec3( noise( iceil( nuv.x ), iceil( cuv.y ), z ) );
    res[2] = vec3( noise( iceil( cuv.x ), iceil( nuv.y ), z ) );
    res[3] = vec3( noise( iceil( nuv.x ), iceil( nuv.y ), z ) );

    return res;
}

void main()
{

    vec2 iuv = uv * 100;
    vec2 next_iuv = uv * 100 + 1;
    if( next_iuv.x >= 100 )
        next_iuv.x = 0;

    vec2 smaller_iuv = iuv * 3.0;
    vec2 smaller_next_iuv = smaller_iuv + 1;
    if( smaller_next_iuv.x >= 300 )
        smaller_next_iuv.x = 0;

    vec3[4] noised_cols1 = generateNoiseRect( iuv, next_iuv, current_perlin_const );
    vec3[4] noised_cols2 = generateNoiseRect( iuv, next_iuv, next_perlin_const );

    vec3[4] smaller_noised_cols1 = generateNoiseRect( smaller_iuv, smaller_next_iuv, current_perlin_const );
    vec3[4] smaller_noised_cols2 = generateNoiseRect( smaller_iuv, smaller_next_iuv, next_perlin_const );

    vec3 perlin_col1 = bi_mix( noised_cols1, fract( iuv ) );
    vec3 perlin_col2 = bi_mix( noised_cols2, fract( iuv ) );

    vec3 smaller_perlin_col1 = bi_mix( smaller_noised_cols1, fract( smaller_iuv ) );
    vec3 smaller_perlin_col2 = bi_mix( smaller_noised_cols2, fract( smaller_iuv ) );

    vec3 perlin_col = mix( perlin_col1, perlin_col2, anim_state ) + 
                      mix( smaller_perlin_col1, smaller_perlin_col2, anim_state );

    out_color = vec4( perlin_col * color, 1 );

    out_color.xyz = ( ( out_color.xyz + vec3( 0.3 / length( out_color.xyz ) ) ) * color );
    if( length( out_color.xyz ) <= 1.1 )
        out_color.xyz = color * 0.6;
}
