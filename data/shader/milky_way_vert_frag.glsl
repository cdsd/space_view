//###vert
#version 330
in vec3 vert;
in vec2 uv;

uniform mat4 camera;
uniform mat4 projection;

out vec2 ex_uv;

void main()
{
    vec4 cam_vec = camera * vec4( vert, 0 );
    gl_Position = projection * vec4( cam_vec.xyz, 1 );
    ex_uv = uv;
}
//###frag
#version 330
in vec2 ex_uv;

uniform sampler2D ttu;

out vec4 color;

void main()
{
    color = vec4( texture( ttu, ex_uv ).xyz, 0.2 );
}
