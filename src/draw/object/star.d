module draw.object.star;

import des.gl.simple;
import des.space;

import utils.appext;
import utils.types.database;

import draw.object.base;
import draw.utils.camera;

import std.math;
import std.random;

class Star : ReloadableGLSimpleObject, SpaceNode
{
    mixin SpaceNodeHelper;
private:
    GLBuffer vert, uv, index;

    uint segments;

    Protuberance[10] protuberances;

    MCamera camera;

    void recalcAndSetData()
    {
        auto radius = state_handler.currentStar.sizeCoef;
        vec3[] vert_data; 
        vec2[] uv_data;
        uint[] index_data;

        auto hsegments = segments / 2;
        auto rsegments = segments + 1;

        foreach( lat; 0 .. hsegments + 1 )
            foreach( lon; 0 .. segments + 1 )
            {
                auto v = cast( float )lat / hsegments;
                auto theta = v * PI;
                auto u = cast( float )lon / segments;
                auto phi = u * PI * 2;

                auto x = radius * sin( theta ) * cos( phi );
                auto y = radius * sin( theta ) * sin( phi );
                auto z = radius * cos( theta );
                vert_data ~= vec3( x, y, z );
                uv_data ~= vec2( u, v );
                if( lat < hsegments && lon < segments + 1 )
                {
                    index_data ~= lat * rsegments + lon;
                    index_data ~= ( lat + 1 ) * rsegments + lon;
                    index_data ~= lat * rsegments + lon + 1;

                    index_data ~= lat * rsegments + lon + 1;
                    index_data ~= ( lat + 1 ) * rsegments + lon;
                    index_data ~= ( lat + 1 ) * rsegments + lon + 1;
                }
            }

        vert.setData( vert_data, GLBuffer.Usage.STATIC_DRAW );
        uv.setData( uv_data, GLBuffer.Usage.STATIC_DRAW );
        index.setData( index_data, GLBuffer.Usage.STATIC_DRAW );
    }

protected:
    override @property string[] shader_files()
    { return ["star_system_star_vert_frag.glsl"]; }

public:
    this( ApplicationStateHandler state_handler, uint segments )
    {
        this.segments = segments;

        super( state_handler );

        camera = state_handler.camera;

        vert = createArrayBuffer();
        setAttribPointer( vert, shader.getAttribLocation( "vert" ), 3, GLType.FLOAT );

        uv = createArrayBuffer();
        setAttribPointer( uv, shader.getAttribLocation( "in_uv" ), 2, GLType.FLOAT );

        index = createIndexBuffer();

        state_handler.preJumpConnect( &recalcAndSetData );

        foreach( ref p; protuberances )
            p = newEMM!Protuberance( state_handler );
    }

    void idle( float dt )
    {
        static const int[10] perlin_consts = [ 13, 22, 292, 11, 2, 3422, 4910, 222, 5009, 12592 ];
        static float time = 0;
        static int current_perlin_const = 0;
        static int next_perlin_const = 1;
        time += dt;
        if( time >= 5.0 )
        {
            current_perlin_const++;
            if( current_perlin_const == 10 )
                current_perlin_const = 0;
            next_perlin_const = current_perlin_const + 1;
            if( next_perlin_const == 10 )
                next_perlin_const = 0;
            time = 0;
        }

        foreach( p; protuberances )
            p.idle( dt );

        shader.setUniform!float( "anim_state", time / 5.0 );
        shader.setUniform!int( "current_perlin_const", perlin_consts[ current_perlin_const ] );
        shader.setUniform!int( "next_perlin_const", perlin_consts[ next_perlin_const ] );
    }

    void draw()
    {
        glEnable( GL_DEPTH_TEST );
        shader.setUniform!mat4( "tr", camera.persp_matrix * camera.resolve( this ) );
        shader.setUniform!col3( "color", state_handler.currentStar.color );

        drawElements( DrawMode.TRIANGLES );

        foreach( p; protuberances )
            p.draw();
        glDisable( GL_DEPTH_TEST );
    }
}

class Protuberance : ReloadableGLSimpleObject, SpaceNode
{
    mixin SpaceNodeHelper; 
private:
    GLBuffer vert;

    MCamera camera;

    vec3[] vert_data;
    vec3[] vels;
    vec3[] accels;

    float time = 0;

    vec3 mass;

    auto sign( float s )
    {
        if( s > 0 )
            return 1;
        else return -1;
    }

    void idleVerts( float dt )
    {
        auto size = state_handler.currentStar.sizeCoef;

        time += dt;

        bool is_all_inside = true;

        foreach( i, ref v; vert_data )
        {
            if( v.len2 >= size * size )
                is_all_inside = false;
            vels[i] += accels[i].e * dt;
            if( vels[i].len >= size / 10.0 )
                vels[i] = vels[i].e * size / 10.0;
            v += vels[i] * dt * 3.0;
            accels[i] = mass - v;
        }
        if( time >= 5.0 )
        {
            mass = random_vec * size;
            if( is_all_inside ) 
                setNewData( 100 );
            time = 0;
        }

        vert.setData( vert_data );
    }

    @property random_vec()
    {
        auto x = uniform( -1.0, 1.0 );
        auto y = uniform( -1.0, 1.0 );
        auto z = uniform( -1.0, 1.0 );

        return vec3( x, y, z ).e;
    }

    void setNewData( uint count )
    {
        vert_data.destroy();
        accels.destroy();
        vels.destroy();
        vert_data.length = count;
        accels.length = count;
        vels.length = count;
        shader.setUniform!col3( "color", state_handler.currentStar.color );

        auto size = state_handler.currentStar.sizeCoef;

        mass = random_vec * size;

        auto pos = random_vec * size;

        foreach( i; 0 .. count )
        {
            vert_data[i] = pos - pos * ( 1 - cast( float )i / count );
            accels[i] = mass - vert_data[i] + random_vec * size / 300.0;
        }
    }

protected:
    override @property string[] shader_files()
    { return ["star_system_star_protuberance_vert_frag.glsl"]; }

public:
    this( ApplicationStateHandler state_handler )
    {
        super( state_handler );
        
        camera = state_handler.camera;

        vert = createArrayBuffer();
        setAttribPointer( vert, shader.getAttribLocation( "vert" ), 3, GLType.FLOAT );
        state_handler.preJumpConnect( { setNewData( 100 ); } );
    }

    void idle( float dt )
    {
        idleVerts( dt ); 
    }

    void draw()
    {
        glPointSize( 2.0 );
        shader.setUniform!mat4( "tr", camera.persp_matrix * camera.resolve( this ) );
        drawArrays( DrawMode.TRIANGLE_STRIP );
        glPointSize( 1.0 );
    }
}
