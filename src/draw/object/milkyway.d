module draw.object.milkyway;

import des.gl.simple;
import des.space;
import des.math.linear;

import std.math;

import draw.object.base;
import draw.utils.camera;

import utils.appext;

class MilkyWay : ReloadableGLSimpleObject, SpaceNode
{
    mixin SpaceNodeHelper;
private:
    GLBuffer vert, uv, index;

    GLTexture tex;

    MCamera camera;

    void setData( float radius, uint segs )
    {
        vec3[] vert_data; 
        vec2[] uv_data;
        uint[] index_data;

        auto hsegs = segs / 2;

        auto rsegs = segs + 1;

        foreach( lat; 0 .. hsegs + 1 )
            foreach( lon; 0 .. segs + 1 )
            {
                auto v = cast( float )lat / hsegs;
                auto theta = v * PI;
                auto u = cast( float )lon / segs;
                auto phi = u * PI * 2;

                auto x = radius * sin( theta ) * cos( phi );
                auto y = radius * sin( theta ) * sin( phi );
                auto z = radius * cos( theta );
                vert_data ~= vec3( x, y, z );
                uv_data ~= vec2( u, v );
                if( lat < hsegs && lon < segs + 1 )
                {
                    index_data ~= lat * rsegs + lon;
                    index_data ~= ( lat + 1 ) * rsegs + lon;
                    index_data ~= lat * rsegs + lon + 1;

                    index_data ~= lat * rsegs + lon + 1;
                    index_data ~= ( lat + 1 ) * rsegs + lon;
                    index_data ~= ( lat + 1 ) * rsegs + lon + 1;
                }
            }

        vert.setData( vert_data, GLBuffer.Usage.STATIC_DRAW );
        uv.setData( uv_data, GLBuffer.Usage.STATIC_DRAW );
        index.setData( index_data, GLBuffer.Usage.STATIC_DRAW );
    }
protected:
    override @property string[] shader_files()
    { return [ "milky_way_vert_frag.glsl" ]; }

public:
    this( ApplicationStateHandler state_handler )
    {
        super( state_handler );

        camera = state_handler.camera;

        vert = createArrayBuffer();
        setAttribPointer( vert, shader.getAttribLocation( "vert" ), 3, GLType.FLOAT );

        uv = createArrayBuffer();
        setAttribPointer( uv, shader.getAttribLocation( "uv" ), 2, GLType.FLOAT );

        index = createIndexBuffer();

        setData( state_handler.spaceSphereRadius, 40 );

        tex = newEMM!GLTexture( GLTexture.Target.T2D );

        tex.setParameter( GLTexture.Parameter.MIN_FILTER, GLTexture.Filter.LINEAR );
        tex.setParameter( GLTexture.Parameter.MAG_FILTER, GLTexture.Filter.LINEAR );

        tex.image = state_handler.getTexture( "galaxy_milky_way.png" );

        self_mtr = mat4 ( 0.487887, 0.62758, 0.606717, 0, 
                          0.0598171, 0.669384, -0.740503, 0, 
                          -0.870853, 0.397574, 0.289044, 0,
                          0, 0, 0, 1 );
    }

    void draw()
    {
        tex.bind();
        shader.setUniform!mat4( "projection", camera.persp_matrix );
        shader.setUniform!mat4( "camera", camera.resolve( this ) );
        drawElements( DrawMode.TRIANGLES );
    }
}
