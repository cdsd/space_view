module draw.object.space;

import des.gl.simple;
import des.math.linear;
import des.space;
import des.util.timer;

import draw.object.base;
import draw.utils.camera;

import utils.types.database;
import utils.appext;

class Space : ReloadableGLSimpleObject, SpaceNode
{
    mixin SpaceNodeHelper;
private:
    GLBuffer vert, col, mag;

    float jump_state = 0;
    float jump_duration;
    StarInfo[] old_stars;
    StarInfo[] new_stars;

    bool got_new_stars = false;

    Timer tm;

    MCamera camera;

    StarInfo calcNewStarInfo( in StarInfo star, vec3 new_star_pos )
    {
        import std.math;
        StarInfo res_star = star;
        auto star_pos = ( res_star.np * res_star.dist ) - new_star_pos;
        res_star.np = star_pos.e;
        res_star.dist = star_pos.len;

        res_star.bright = res_star.absBright + 5 * log10( res_star.dist / 10.0 );//Какая то формула из интернета
        res_star.bright = -res_star.bright + 7;
        return res_star;
    }

    void setStarsData( StarInfo[] stars, size_t old_length, vec3 first_offset = vec3( 0 ), vec3 second_offset = vec3(0) )
    {
        vec3[] vert_data; 
        float[] mag_data; 
        col3[] col_data;

        foreach( i, s; stars )
        { 
            if( i < old_length )
                s = calcNewStarInfo( s, first_offset );
            else
                s = calcNewStarInfo( s, second_offset );
            if( s.bright <= 1 || s.bright > 20 ) continue;
            if( s.np.len2 == 0 || !s.np )
                continue;
            vert_data ~= s.np * state_handler.spaceSphereRadius;
            mag_data ~= s.bright;
            col_data ~= s.color;
        }

        vert.setData( vert_data, GLBuffer.Usage.STATIC_DRAW );
        mag.setData( mag_data, GLBuffer.Usage.STATIC_DRAW );
        col.setData( col_data, GLBuffer.Usage.STATIC_DRAW );
    }
    
    void setDataInJump()
    {
        auto offset = state_handler.targetStar.np * state_handler.targetStar.dist * jump_state / jump_duration;
        setStarsData( old_stars ~ new_stars, old_stars.length, offset, offset - state_handler.targetStar.np * state_handler.targetStar.dist );
    }

protected:
    override @property string[] shader_files()
    { return [ "space_plane_vert_geom.glsl", "star_frag.glsl" ]; }

public:
    this( ApplicationStateHandler state_handler )
    {
        super( state_handler );
        camera = state_handler.camera;

        jump_duration = state_handler.warpJumpDuration;

        vert = createArrayBuffer();
        setAttribPointer( vert, shader.getAttribLocation( "vert" ), 3, GLType.FLOAT );

        col = createArrayBuffer();
        setAttribPointer( col, shader.getAttribLocation( "color" ), 3, GLType.FLOAT );

        mag = createArrayBuffer();
        setAttribPointer( mag, shader.getAttribLocation( "mag" ), 1, GLType.FLOAT );
    } 

    void idle( float dt )
    {
        if( jump_state >= jump_duration )
        {
            state_handler.stopWarpJump();
            got_new_stars = false;
            jump_state = 0;
            setStarsData( new_stars, new_stars.length );
        }
        if( state_handler.isInWarpJump && 
            !state_handler.isFirstJump && 
            got_new_stars )
        {
            jump_state += dt;
            setDataInJump();
        }
    }

    void draw()
    {
        shader.setUniform!mat4( "projection", camera.persp_matrix( true ) );
        shader.setUniform!mat4( "camera", camera.resolve( this ) );
        drawArrays( DrawMode.POINTS );
    }

    void setData( StarInfo[] stars )
    {
        if( state_handler.isFirstJump )
        {
            setStarsData( stars, stars.length );
            new_stars = stars;
            state_handler.gotFirstJump();
            state_handler.stopWarpJump();
            return;
        }

        auto dist = state_handler.targetStar.dist;
        if( dist < 1000.0 )
            jump_duration = dist / 100.0;
        else
            jump_duration = dist / 1000.0;

        old_stars = new_stars;
        new_stars = stars;

        got_new_stars = true;
    }
}
