module draw.object.star_system;

import draw.object.star;

import utils.appext;

import des.util.arch.emm;

class StarSystem : ExternalMemoryManager
{
    mixin EMM;
private:
    Star star;

    ApplicationStateHandler state_handler;
public:
    this( ApplicationStateHandler state_handler )
    {
        auto segments = 50;
        this.state_handler = state_handler;
        star = newEMM!Star( state_handler, segments );
    }

    void idle( float dt )
    {
        star.idle( dt ); 
    }

    void draw()
    {
        if( state_handler.isInWarpJump )
            return;
        star.draw();
    }
}
