module draw.object.base;

import des.gl.simple;

import utils.appext;

class ReloadableGLSimpleObject : GLSimpleObject
{
protected:
    ApplicationStateHandler state_handler;

    CommonGLShaderProgram renewShaderProgram()
    {
        GLShader[] s;
        foreach( sf; shader_files )
            s ~= state_handler.getShaders( sf );
        return new CommonGLShaderProgram( s );
    }

    abstract @property string[] shader_files();
public:
    this( ApplicationStateHandler state_handler )
    {
        state_handler.addReloadableObject( this );
        this.state_handler = state_handler;
        super( registerChildEMM( renewShaderProgram() ) );
    }

    void reloadShaders()
    {
        shader.destroy();
        shader = registerChildEMM( renewShaderProgram() );
    }
}
