module draw.object.gui.mark;

import des.gl.simple;
import des.math.linear;
import des.space;
import des.app.event;

import draw.object.base;
import draw.utils.camera;

import utils.types.database;
import utils.appext;

class Cursor : ReloadableGLSimpleObject
{
private:
    GLBuffer vert;

    vec2 win_size;

    void repos( ivec2 sz )
    {
        win_size = vec2( sz );
        vec2 cursor_size = vec2( 30, 20 );
        vec2[] vert_data;
        vert_data ~= vec2( sz ) / 2.0 - cursor_size;
        vert_data ~= vec2( sz ) / 2.0 - cursor_size / 3.0;

        vert_data ~= vec2( sz ) / 2.0 + cursor_size;
        vert_data ~= vec2( sz ) / 2.0 + cursor_size / 3.0;

        vert_data ~= vec2( sz ) / 2.0 + vec2( cursor_size.x, -cursor_size.y );
        vert_data ~= vec2( sz ) / 2.0 + vec2( cursor_size.x, -cursor_size.y ) / 3.0;

        vert_data ~= vec2( sz ) / 2.0 + vec2( -cursor_size.x, cursor_size.y );
        vert_data ~= vec2( sz ) / 2.0 + vec2( -cursor_size.x, cursor_size.y ) / 3.0;

        vert.setData( vert_data );
    }

protected:
    override @property string[] shader_files()
    { return ["cursor_vert_frag.glsl"]; }

public:
    this( ApplicationStateHandler state_handler )
    {
        state_handler.windowResizeConnect( &repos );
        super( state_handler );

        vert = createArrayBuffer();
        setAttribPointer( vert, shader.getAttribLocation( "vert" ), 2, GLType.FLOAT );
    }

    void draw()
    {
        glEnable( GL_LINE_SMOOTH );
        glLineWidth( 2 );
            shader.setUniform!vec2( "win_size", vec2( win_size ) );
            shader.setUniform!col4( "color", col4( 0.7, 0.7, 0.8, 0.6 ) );
            drawArrays( DrawMode.LINES );
        glLineWidth( 1 );
        glDisable( GL_LINE_SMOOTH );
    }
}

class Mark : ReloadableGLSimpleObject, SpaceNode
{
    mixin SpaceNodeHelper;
private:
    GLBuffer vert, mag;

    Cursor cursor;

    MCamera camera;

protected:
    override @property string[] shader_files()
    { return ["space_plane_vert_geom.glsl", "mark_frag.glsl"]; }

public:
    this( ApplicationStateHandler state_handler )
    {
        cursor = newEMM!Cursor( state_handler );

        super( state_handler );

        camera = state_handler.camera;

        vert = createArrayBuffer();
        setAttribPointer( vert, shader.getAttribLocation( "vert" ), 3, GLType.FLOAT );

        mag = createArrayBuffer();
        setAttribPointer( mag, shader.getAttribLocation( "mag" ), 1, GLType.FLOAT );
    }

    void draw()
    {
        if( state_handler.isInWarpJump || state_handler.isInStarSystemJump )
            return;
        cursor.draw();
        if( state_handler.targetStar.name == "__undefined__" )
            return;
        setData( state_handler.targetStar );
        shader.setUniform!mat4( "camera", camera.resolve( this ) );
        shader.setUniform!mat4( "projection", camera.persp_matrix( true ) );
        drawArrays( DrawMode.POINTS );
    }

    void setData( StarInfo star )
    {
        if( star.name == "__undefined__" )
            return;
        vert.setData( [ star.np * state_handler.spaceSphereRadius ] );
        mag.setData( [ 10f ] );
    }

    void mouseControl( in MouseEvent ev )
    {
        if( state_handler.isInWarpJump || state_handler.isInStarSystemJump )
            return;
        if( ev.type == ev.Type.MOTION )
        {
            auto ray = camera.resolve( this ).inv * camera.persp_matrix.inv * vec4( 0, 0, 1, 1 );
            state_handler.mouseRay = vec3( ray.xyz.e );
        }
        if( ev.type == ev.Type.PRESSED && ev.btn == ev.Button.RIGHT )
            state_handler.jump( state_handler.targetStar );
    }
}
