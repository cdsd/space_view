module draw.window;

public import des.app;
import des.flow.event;
import des.gl.simple;
import des.il.region;
import des.util.timer;

import utils.appext;
import utils.types.database;

import draw.object.space;
import draw.object.milkyway;
import draw.object.gui.mark;
import draw.object.star_system;

class MainWindow : DesWindow
{
private:
    ApplicationStateHandler state_handler;

    Space space;
    MilkyWay milky_way;
    StarSystem star_system;
    Mark mark;

    BaseLineTextBox fps_text;
    BaseLineTextBox loading_text;
    BaseLineTextBox target_star_text;

    Timer tm;

    bool is_loading = true;

    void keyboardFunc( in KeyboardEvent ev )
    {
        switch( ev.scan )
        {
            case ev.Scan.ESCAPE:
                app.quit();
                break;
            case ev.Scan.R:
                state_handler.reloadShaders();
                break;
            default:
                break;
        }
    }

    void mouseFunc( in MouseEvent ev )
    {
        state_handler.camera.mouseControl( ev ); 
        mark.mouseControl( ev );
    }

    void idleFunc()
    {
        auto dt = tm.cycle();

        space.idle( dt );
        star_system.idle( dt );
        state_handler.camera.idle( dt );
        debug
        {
            import std.conv;
            static float time = 0;
            time += dt;
            if( time > 0.3 )
            {
                time = 0;
                fps_text.text = to!wstring( cast(uint)(1 / dt) );
            }
        }

        target_star_text.text = ( state_handler.targetStar.name != ""  ? state_handler.targetStar.name : "Unknown" ) ~ 
                               ":dist - " ~ to!string( state_handler.targetStar.dist ) ~ "pk";
    }

    void drawFunc()
    {
        glDisable( GL_DEPTH_TEST );
        milky_way.draw();
        space.draw();
        star_system.draw();
        debug
            fps_text.draw( size );
        if( !is_loading && !state_handler.isInWarpJump && !state_handler.isInStarSystemJump )
        {
            mark.draw();
            if( state_handler.targetStar.name != "__undefined__" )
                target_star_text.draw( size );
        }
        if( is_loading )
            loading_text.draw( size );
    }

    void resizeFunc( ivec2 sz )
    {
        state_handler.camera.ratio = cast( float )sz.x / sz.y;
        state_handler.windowResize( sz );
        loading_text.position = vec2( sz ) / 2.0 - loading_text.size / 2;
        fps_text.position = vec2( sz.x, 10 ) - vec2( 50, 0 );
    }

    void prepareGL()
    {
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glEnable( GL_DEPTH_TEST );
        glEnable( GL_PROGRAM_POINT_SIZE );
    }

    void prepareTexts()
    {
        fps_text = newEMM!BaseLineTextBox( state_handler.defaultFont );
        fps_text.color = col3( 0.9 );

        loading_text = newEMM!BaseLineTextBox( state_handler.defaultFont, 54 );
        loading_text.text = "Loading...";
        loading_text.color = col3( 0.9 );

        target_star_text = newEMM!BaseLineTextBox( state_handler.defaultFont );
        target_star_text.position = vec2( 10, 10 );
        target_star_text.color = col3( 0.9 );
    }

protected:
    override void prepare()
    {
        prepareGL();
        prepareTexts();
        space = newEMM!Space( state_handler );
        milky_way = newEMM!MilkyWay( state_handler );
        mark = newEMM!Mark( state_handler );
        star_system = newEMM!StarSystem( state_handler );

        tm = new Timer;

        connect( key, &keyboardFunc );
        connect( mouse, &mouseFunc );
        connect( draw, &drawFunc );
        connect( idle, &idleFunc );
        connect( event.resized, &resizeFunc );
    }
public:
    this( ApplicationStateHandler state_handler )
    { 
        this.state_handler = state_handler;
        super( "SpaceView", ivec2( 800, 600 ), false ); 
    }

    void setStars( StarInfo[] stars )
    { 
        is_loading = false;
        space.setData( stars ); 
    }
}
