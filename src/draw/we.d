module draw.we;

import des.flow;
import des.flow.signal:Signal;
import des.util.arch.base;
import des.util.helpers;

import utils.yamlext;
import utils.flowext;
import utils.types.database;
import utils.types.codes;
import utils.appext;

import draw.window;
import draw.utils.camera;

class DrawWE : WorkElement, EventProcessor
{
private:
    DesApp application;
    MainWindow window;
    FlowStateHandler fstate_handler;
    ApplicationStateHandler astate_handler;

    void prepareAppStateHandler( in YAMLNode set )
    { 
        astate_handler.mouseRayCallback = ( vec3 ray )
        { pushEvent( Event( EvCode.GET_STAR_INFO_BY_RAY, ray ) ); };

        astate_handler.preJumpConnect( { pushEvent( Event( EvCode.GET_STARS, []) ); } );
    }

public:
    this( in YAMLNode set, FlowStateHandler fstate_handler, ApplicationStateHandler astate_handler )
    {
        this.fstate_handler = fstate_handler;
        this.astate_handler = astate_handler;

        prepareAppStateHandler( set );

        application = newEMM!DesApp;
        application.addWindow( { return window = newEMM!MainWindow( astate_handler ); } );
    }

    override void process()
    {
        if( astate_handler.isFirstRun )
        {
            StarInfo sun = astate_handler.getStarInfo( 0 );
            astate_handler.jump( sun );
        }
        application.step();
        if( !application.isRunning )
            sendSignal( Signal( SigCode.QUIT ) );
    }

    override EventProcessor[] getEventProcessors() 
    { return cast(EventProcessor[])([this, astate_handler]); }

    override void processEvent( in Event ev )
    { 
        switch( ev.code )
        {
            case EvCode.STARS:
                auto stars = ev.as!(StarInfo[]);
                window.setStars( stars );
                break;
            default:
                break;
        }

    }
}
