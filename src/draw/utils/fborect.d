module draw.utils.fborect;

import des.gl;

class FBORect: GLObject
{
private:
    enum SS_BASE_FBO_VERT = 
```//###vert
#version 330
in vec4 vertex;
in vec2 in_uv;

out vec2 uv;

void main()
{
    gl_Position = vertex;
    uv = in_uv;
}

```;
private:
    GLFrameBuffer fbo;
    GLBuffer pos, uv;
    ivec2 wsz = ivec2(800,800);

    auto internal_format = GLTexture.InternalFormat.RGBA;
    auto format = GLTexture.Format.RGBA;
    auto type = GLTexture.Type.FLOAT;

    GLTexture tex;

    void resizeFbo()
    {
        tex.image( wsz, internal_format, format, type );
        tex.genMipmap();
    }
public:
    CommonGLShaderProgram shader;

    this( GLShader frag_shader )
    in{ assert( frag_shader.type == GLShader.Type.FRAGMENT ); }
    body
    {
        auto vert_shader = parseGLShaderSource( SS_BASE_FBO_VERT );
        shader = newEMM!CommonGLShaderProgram( vert_shader ~ frag_shader );

        fbo = newEMM!GLFrameBuffer;

        tex = newEMM!GLTexture( GLTexture.Target.T2D );

        tex.setParameter( GLTexture.Parameter.MIN_FILTER, GLTexture.Filter.NEAREST );
        tex.setParameter( GLTexture.Parameter.MAG_FILTER, GLTexture.Filter.NEAREST );

        resizeFbo();

        int pos_loc = shader.getAttribLocation( "vertex" );
        int uv_loc = shader.getAttribLocation( "in_uv" );

        auto pos_dt = [ vec2(-1, 1), vec2(1, 1), vec2(-1,-1), vec2(1,-1) ];
        auto uv_dt =  [ vec2( 0, 1), vec2(1, 1), vec2( 0, 0), vec2(1, 0) ];

        pos = newEMM!GLBuffer( GLBuffer.Target.ARRAY_BUFFER );
        setAttribPointer( pos, pos_loc, 2, GLType.FLOAT );
        uv = newEMM!GLBuffer( GLBuffer.Target.ARRAY_BUFFER );
        setAttribPointer( uv, uv_loc, 2, GLType.FLOAT );

        pos.setData( pos_dt, GLBuffer.Usage.STATIC_DRAW );
        uv.setData( uv_dt, GLBuffer.Usage.STATIC_DRAW );

        fbo.texture( tex, GLFrameBuffer.Attachment.COLOR0 );
    }

    void bind(bool clear=true)
    { 
        fbo.bind();
        if( clear )
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    }

    void resize( in ivec2 sz )
    {
        wsz = sz;
        resizeFbo();
    }

    void unbind() { fbo.unbind(); }

    void preDraw()
    {
        vao.bind();
        shader.use();
        shader.setUniform!vec2( "win_size", vec2(wsz) );
    }

    void draw()
    {
        tex.bind();
        glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
    }
}
