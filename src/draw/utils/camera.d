module draw.utils.camera;

import std.math;

import des.space;
import des.app.event;

import utils.appext;

class MCamera : Camera
{
protected:
    LookAtTransform look_tr;

    ApplicationStateHandler state_handler;

    vec2 rot;
    vec2 object_rot;

    vec3 warp_jump_eye;
    vec3 warp_jump_dvec;

    float y_angle_limit = PI_2 - 0.001;

    float fov_min_angle = 50;
    float fov_max_angle = 100;

    float jump_duration;
    float jump_distance = 100.0;
    float camera_to_star_min_distance = 2.0;

    void resetLookTr()
    {
        look_tr.target = vec3(0,0,0);
        look_tr.pos = vec3(1,0,0);
        look_tr.up = vec3(0,0,1);
    }

    void inStarSystemAnimation( float dt )
    {
        if( !state_handler.isInStarSystemJump )
            return;
        static float time = 0;
        time += dt;

        look_tr.pos -= look_tr.target;
        look_tr.target = vec3( 0 );

        void stopAnimation()
        {
            time = 0;
            state_handler.stopStarSystemJump();
        }

        if( time >= jump_duration )
        {
            stopAnimation();
            return;
        }
        
        auto anim_state = 1 - time / jump_duration;

        import std.math;
        auto to_star_distance = anim_state * jump_distance * state_handler.currentStar.sizeCoef;
        if( to_star_distance <= camera_to_star_min_distance * state_handler.currentStar.sizeCoef )
        {
            stopAnimation();
            return;
        }

        look_tr.pos = look_tr.pos.e * to_star_distance;

        //auto q = quat.fromAngle( dt / jump_duration, vec3( 0, 0, 1 ) );
        //look_tr.pos = q.rot( look_tr.pos );
    }

    void inWarpJumpAnimation( float dt )
    {
        static float time;
        if( !state_handler.isInWarpJump )
        {
            time = 0;
            return;
        }
        if( state_handler.currentStar.np.len == 0 )
            return;

        time += dt;
        if( time >= state_handler.warpJumpDuration / 2.0 )
        {
            look_tr.target = look_tr.pos + state_handler.currentStar.np;
            return;
        }

        look_tr.target = look_tr.pos + warp_jump_eye + warp_jump_dvec * time / ( state_handler.warpJumpDuration / 2.0 );
    }

public:
    PerspectiveTransform perspective;
    PerspectiveTransform perspective_geom;

    this( ApplicationStateHandler state_handler )
    {
        super();
        this.state_handler = state_handler;

        jump_duration = state_handler.inStarSystemJumpDuration;

        look_tr = new LookAtTransform;
        resetLookTr();
        transform = look_tr;

        auto eye_vec = look_tr.target - look_tr.pos;
        rot.x = atan2( eye_vec.y, eye_vec.x );

        perspective = new PerspectiveTransform; 
        perspective.near = 0.01;
        projection = perspective;

        perspective_geom = new PerspectiveTransform; 
        perspective_geom.near = float.min_normal;//TODO Ошибки в геометрическом шейдере space при бОльших значениях!!!

        state_handler.preJumpConnect( 
        {
            warp_jump_eye = look_tr.target - look_tr.pos;
            warp_jump_dvec = state_handler.currentStar.np * warp_jump_eye.len - warp_jump_eye;
        });
    }

    void idle( float dt )
    {
        inWarpJumpAnimation( dt );
        inStarSystemAnimation( dt );
    }

    void mouseControl( in MouseEvent ev )
    {
        if( ( state_handler.isInWarpJump || state_handler.isInStarSystemJump ) && !state_handler.isFirstJump )
            return;
        if( ev.isPressed( ev.Button.LEFT ) || ev.isPressed( ev.Button.MIDDLE ) )
        {
            rot += vec2( ev.rel ) / 1000.0;
            object_rot = vec2( rot.x + PI, -rot.y );
            normalizeRotation();
            if( ev.isPressed( ev.Button.LEFT ) )
                lookAround();
            if( ev.isPressed( ev.Button.MIDDLE ) )
                lookAroundObject();
        }

        if( ev.type == ev.Type.WHEEL )
        {
            perspective.fov = perspective.fov - ev.whe.y;
            normalizeFOV();
            perspective_geom.fov = perspective.fov;
        }
    }

    @property 
    {
        mat4 persp_matrix( bool for_geom = false )
        {  
            if( !for_geom )
                return perspective.matrix;
            return perspective_geom.matrix;
        }

        void ratio( float r )
        { 
            perspective.ratio = r;
            perspective_geom.ratio = r;
        }
    }
private:

    void normalizeRotation()
    {
        if( rot.y > y_angle_limit )
            rot.y = y_angle_limit;
        if( rot.y < -y_angle_limit )
            rot.y = -y_angle_limit;

        if( object_rot.y > y_angle_limit )
            object_rot.y = y_angle_limit;
        if( object_rot.y < -y_angle_limit )
            object_rot.y = -y_angle_limit;
    }

    void normalizeFOV()
    {
        if( perspective.fov > fov_max_angle )
            perspective.fov = fov_max_angle;
        if( perspective.fov < fov_min_angle )
            perspective.fov = fov_min_angle;
    }

    vec3 calcOrbit( vec2 angles )
    {
        return vec3( cos( angles.x ) * cos( angles.y ),
                     sin( angles.x ) * cos( angles.y ),
                     sin( angles.y ) );
    }

    void lookAround()
    {
        auto nvec = calcOrbit( rot ) * ( look_tr.target - look_tr.pos ).len;
        look_tr.target = look_tr.pos + nvec;
    }

    void lookAroundObject()
    {
        auto nvec = calcOrbit( object_rot ) * ( look_tr.target - look_tr.pos ).len;
        look_tr.pos = look_tr.target + nvec;
    }
}
