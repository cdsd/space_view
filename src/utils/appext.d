module utils.appext;

import draw.utils.camera;
import draw.object.base;

import calc.starparse;

import des.il.image;
import des.util.arch;
import des.util.arch.sig : Signal;
import des.util.helpers;
import des.gl.base;
import des.flow;

import utils.types.database;
import utils.types.codes;
import utils.imageext;

class ApplicationStateHandler : DesObject, EventProcessor
{
    mixin DES;
private:
    string app_data_path;
    string config_file_name;
    string shader_folder_name;
    string texture_folder_name;
    string database_folder_name;
    string font_folder_name;

    bool first_run = true;
    bool first_jump = true;

    bool is_in_warp_jump = false;
    bool is_in_star_system_jump = false;

    uint space_sphere_radius = 100;
    float warp_jump_duration = 2.0;
    float in_star_system_jump_duration = 2.0;

    void delegate( vec3 ) mouse_ray_callback;
    StarInfo delegate( size_t ) star_by_id_callback;

    StarInfo target_star;
    StarInfo current_star;

    StarInfo[] _jumps;

    void resetTargetStar()
    {
        StarInfo star;
        star.name = "__undefined__";
        target_star = star;
    }

    ReloadableGLSimpleObject[] reloadable_objects;

public:
    MCamera camera;

    Signal!ivec2 windowResize;
    Signal!() preJump;

    this( string app_data_path, string config_file_name,
          string shader_folder_name, string texture_folder_name,
          string database_folder_name, string font_folder_name )
    {
        this.camera = new MCamera( this );
        this.app_data_path = app_data_path;
        fixAppDataPath();
        this.config_file_name = config_file_name;
        this.shader_folder_name = shader_folder_name;
        this.texture_folder_name = texture_folder_name;
        this.database_folder_name = database_folder_name;
        this.font_folder_name = font_folder_name;
    }

    GLShader[] getShaders( string shader_file_name )
    {
        import std.file;
        auto source = readText( app_data_path ~ bnPath( shader_folder_name, shader_file_name ) );
        return parseGLShaderSource( source );
    }

    Image!2 getTexture( string texture_file_name )
    { return loadImageFromFile( app_data_path ~ bnPath( texture_folder_name, texture_file_name ) ); }

    SpaceDB getDatabase( string database_file_name )
    {
        auto database = app_data_path ~ bnPath(database_folder_name, database_file_name );
        return new SpaceDB( database );
    }

    void addReloadableObject( ReloadableGLSimpleObject obj )
    { reloadable_objects ~= obj; }

    void reloadShaders()
    {
        foreach( r; reloadable_objects )
            r.reloadShaders();
    }

    void gotFirstRun(){ first_run = false; }
    void gotFirstJump(){ first_jump = false; }

    void stopWarpJump()
    { 
        is_in_warp_jump = false;
        is_in_star_system_jump = true;
        resetTargetStar();
    }

    void stopStarSystemJump()
    { is_in_star_system_jump = false; }

    void windowResizeConnect( void delegate( ivec2 ) cb )
    { connect( windowResize, cb ); }

    void preJumpConnect( void delegate() cb )
    { connect( preJump, cb ); }

    @property
    {
        uint spaceSphereRadius(){ return space_sphere_radius; }

        void mouseRayCallback( void delegate( vec3 ) cb )
        { mouse_ray_callback = cb; }
        void mouseRay( vec3 v )
        { mouse_ray_callback( v ); }

        void starByIdCallback( StarInfo delegate( size_t ) cb )
        { star_by_id_callback = cb; }

        StarInfo targetStar(){ return target_star; }

        auto config(){ return app_data_path ~ config_file_name; }

        auto currentStar(){ return current_star; }

        bool isFirstRun(){ return first_run; }
        bool isFirstJump(){ return first_jump; }

        bool isInWarpJump(){ return is_in_warp_jump; }
        bool isInStarSystemJump(){ return is_in_star_system_jump; }

        float warpJumpDuration(){ return warp_jump_duration; }
        float inStarSystemJumpDuration(){ return in_star_system_jump_duration; }

        StarInfo[] jumps(){ return _jumps; }
        StarInfo lastJump()
        { 
            import std.array;
            return jumps.back();
        }

        string defaultFont(){ return bnPath( app_data_path, font_folder_name, "default.ttf" ); }
    }

    void jump( StarInfo star )
    { 
        if( star.name == "__undefined__" )
            return;
        current_star = star;
        _jumps ~= star;
        is_in_warp_jump = true;
        preJump();
    }

    StarInfo getStarInfo( size_t id )
    { return star_by_id_callback( id ); }

    override void processEvent( in Event ev )
    { 
        switch( ev.code )
        {
            case EvCode.STAR_INFO_BY_RAY:
                auto star = ev.as!StarInfo;
                if( star.name != "__undefined__" )
                    target_star = ev.as!StarInfo;
                break;
            default:
                break;
        }
    }

private:
    void fixAppDataPath()
    {
        import std.array;
        import std.path;
        import std.conv;
        if( app_data_path.back != to!char( dirSeparator ) )
            app_data_path ~= dirSeparator;
    }
}
