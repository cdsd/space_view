module utils.imageext;

import derelict.devil.il;
import derelict.devil.ilu;

import des.il.image;
import des.util.data.type;
import des.util.stdext.string;
import des.math.linear;

import std.string;

class ImageException : Exception 
{ 
    this( string msg, string file=__FILE__, size_t line=__LINE__ ) @safe pure nothrow
    { super( msg, file, line ); }
}

static this()
{
    DerelictIL.load();
    DerelictILU.load();
    ilInit();
    iluInit();
    ilEnable( IL_FILE_OVERWRITE );
}

Image!2 loadImageFromFile( string fname )
{
    import std.file;
    if( !exists( fname ) )
        throw new ImageException( "No such file: " ~ fname );

    ILuint im;
    ilGenImages( 1, &im );
    scope(exit) ilDeleteImages( 1, &im );
    ilBindImage( im );

    if( ilLoadImage( fname.toStringz ) == false )
        throw new ImageException( "ilLoadImage fails: " ~ 
                                    toDString( iluErrorString( ilGetError() ) ) );

    int w = ilGetInteger( IL_IMAGE_WIDTH );
    int h = ilGetInteger( IL_IMAGE_HEIGHT );
    int c = ilGetInteger( IL_IMAGE_BYTES_PER_PIXEL );

    ubyte* raw = ilGetData();
    ubyte[] data;
    data.length = w * h * c;
    
    foreach( i, ref d; data ) d = raw[i];

    return Image!2( ivec2( w, h ), ElemInfo( DataType.UBYTE, c ), data );
}
