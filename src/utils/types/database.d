module utils.types.database;

import des.math.linear;

struct StarInfo
{
    size_t id;
    string name = "__undefined__";
    vec3 np; // norm position
    float dist; // distance in parsecs
    float bright;
    float absBright;
    col3 color;
    immutable  (float)[] spectr;
    immutable (size_t)[] avail_dst_stars;

    bool opCast(E)() const if( is( E == bool ) )
    { return name != "__undefined__"; }

    @property sizeCoef()
    {
        auto max_abs_bright = 20;//find out from database
        return -( absBright - max_abs_bright );
    }
}

struct AvailStar
{
    size_t src_star;
    size_t dst_star;
    @property id(){ return src_star; }
}

struct RingInfo
{
    string img;
    float[2] radius;
    vec2 angle;
}

struct PlanetInfo
{
    size_t id;
    size_t pid; //parent id
    string name;
    vec3 np; //coordinates relative to parent
    float dist; //relative to parent
    float size;
    string desc; //short description
    string img; //path to image

    RingInfo ring;

    immutable(SatelliteInfo)[] satellites;

    enum float rad_coef = 0.000001f;
    enum float pos_coef = 14.96f;
}

struct SatelliteInfo
{
    size_t id;
    size_t pid; //parent id
    string name;
    vec3 np; //coordinates relative to parent
    float dist; //relative to parent
    float size;
    string desc; //short description
    string img; //path to image
    string mesh; //path to mesh

    enum float rad_coef = 0.000001f;
    enum float pos_coef = 14.96f;
}

struct ConstellationInfo
{
    size_t id;
    string name;
    string desc;
}

struct ConstellationPartInfo
{
    size_t id;
    size_t pid;
    vec3 p1;
    vec3 p2;
}
