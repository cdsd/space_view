module utils.types.codes;

enum SigCode
{
    QUIT,
    RELOAD_SETTINGS
}

enum EvCode
{
    //App codes
    SETTINGS,

    //Calc/draw codes
    GET_STARS,
    GET_STAR_INFO_BY_RAY,

    STARS,
    STAR_INFO_BY_RAY
}
