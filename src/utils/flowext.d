module utils.flowext;

public import des.flow;
import des.util.arch.emm;

import utils.appext;
import utils.yamlext;
import utils.types.codes;

import core.thread;

class FlowStateHandler : ExternalMemoryManager
{
    mixin EMM;

private:
    ApplicationStateHandler astate_handler;

    FThread[string] thread_list;

    auto getConfigRoot()
    { return yaml.Loader( astate_handler.config ).load(); }

public:
    this( ApplicationStateHandler astate_handler )
    { this.astate_handler = astate_handler; }

    void prepareThreads( WorkElement function( in YAMLNode, 
                                               FlowStateHandler, 
                                               ApplicationStateHandler )[] we_funcs, 
                         string[] config_roots )
    in{ assert( we_funcs.length == config_roots.length ); }
    out{ assert( thread_list.length == we_funcs.length ); }
    body
    {
        auto root = getConfigRoot();
        foreach( i, we_func; we_funcs )
        {   
            auto config_root = config_roots[i];
            const auto set = root[config_root];//Что за хуйня!!!!
            thread_list[ config_root ] = new FThread( config_root, we_func, 
                                                      set, this, astate_handler );
        }
    }

    void addListener( string whom, string who )//Кого, кто
    { thread_list[ whom ].addListener( thread_list[ who ] ); }

    bool step()
    {
        foreach( thread; thread_list )
        {
            foreach( signal; thread.takeAllSignals() )
            {
                auto sigcode = cast(SigCode)signal.code;
                final switch( sigcode )
                {
                    case SigCode.QUIT:
                        pushCommand( Command.CLOSE );
                        return false;
                    case SigCode.RELOAD_SETTINGS:
                        reloadSettings();
                        break;
                }
            }
            if( thread.info.error != FThread.Error.NONE )
            {
                logger.error( "'%s' has error [%s] at time [%016.9f] : %s",
                        thread.name, thread.info.error, 
                        thread.info.timestamp / 1e9f, thread.info.message );
                pushCommand( Command.CLOSE );
                return false;
            }
        }
        return true;
    }

    void run()
    {
        pushCommand( Command.START );
        while( step() )
            Thread.sleep( dur!"msecs"(100) );
        pushCommand( Command.CLOSE );
        join();
        destroy();
    }

    void pushCommand( Command command )
    { foreach( thread; thread_list ) thread.pushCommand( command ); }

    void join(){ foreach( thread; thread_list ) thread.join(); }

    void reloadSettings()
    {
        auto root = getConfigRoot();

        foreach( thread; thread_list )
        {
            auto dumped_settings = dumpYAML( root[thread.name] );
            thread.pushEvent( Event( EvCode.SETTINGS, dumped_settings ) );
        }
    }

}
