module calc.starparse; 

import etc.c.sqlite3; 

import std.string;
import std.array;

import des.util;
import des.math.linear;

import utils.types.database;

class SQLiteApiException : Exception
{
    @safe pure nothrow this( string msg, string file = __FILE__, size_t line = __LINE__ )
    { super( msg, file, line ); }
}

class DBException : Exception
{
    @safe pure nothrow this( string msg, string file = __FILE__, size_t line = __LINE__ )
    { super( msg, file, line ); }
}

class DBNoDataException : DBException
{
    @safe pure nothrow this( string msg, string file = __FILE__, size_t line = __LINE__ )
    { super( msg, file, line ); }
}

class DBMultipleReturnDataException : DBException
{
    @safe pure nothrow this( string msg, string file = __FILE__, size_t line = __LINE__ )
    { super( msg, file, line ); }
}

class SQLiteApi : ExternalMemoryManager
{
    mixin EMM;
private:
    sqlite3* db;

    string[] retval;

    extern(C) static int cb ( void *_db, int argc, char **argv, char **azColName )
    {
        string res;
        auto mdb = cast(SQLiteApi)_db;
        for( int i = 0; i < argc; i++ )
        {
            if( i < argc - 1 )
                res ~= argv[i].toDString ~ "|";
            else
                res ~= argv[i].toDString;
        }
        mdb.retval ~= res;
        return 0;
    }

public:
    this( string fname )
    {
        import std.file;
        if( !exists( fname ) )
            throw new SQLiteApiException( "No such file: " ~ fname );
        if( sqlite3_open( fname.toStringz, &db ) )
            throw new SQLiteApiException( "Unable to load db: " ~ sqlite3_errmsg(db).toDString );
    }

    protected void selfDestroy()
    {
        if( db !is null )
            sqlite3_close( db );
    }

    string[] call( string req )
    {
        retval.length = 0;
        char* err;
        scope(exit) sqlite3_free(err);
        if( sqlite3_exec( db, req.toStringz, &cb, cast(void*)this, &err) != SQLITE_OK )
        {
            import std.string;
            throw new SQLiteApiException( format("Error in request \"%s\":%s",
                        req, err.toDString ) );
        }
        return retval.dup; 
    }
}

private abstract class ObjTable(T)
{
protected:

    abstract
    {
        @property string table_name()const;
        T parse( string s );
    }

    size_t[string] cols;

    SQLiteApi db;

public:

    this( SQLiteApi _db )
    {
        if( _db is null )
            throw new DBException( "Null api object." );
        db = _db;
        auto res = db.call( format("select count(*) from sqlite_master where type = 'table' and name = '%s'",
                     table_name) );
        if( res[0] == "0" )
            throw new DBException( "No such table " ~ table_name );

        res = db.call( format("select sql from sqlite_master where tbl_name='%s' and type='table'", table_name ));

        auto rr = res[0];
        rr = rr[ rr.indexOf("(")+1 .. $ ];
        auto sp = rr.split(",");
        foreach( i, ref s; sp )
        {
            s = s.strip();
            auto ssr = s.split();
            cols[ssr[0]] = i;
        }
    }

    T opIndex( size_t ind )
    {
        auto ret = db.call( format( "select * from %s where id like %d;", table_name, ind ) );
        if( ret.length > 1 )
            throw new DBMultipleReturnDataException( format("Multiple values for id %d", parse(ret[0]).id) );
        return parse( ret[0] );
    }

    //T[] nextRange( size_t r )
    //{
    //    T[] res;
    //    auto end = r;
    //    if( cur + r >= objs.length )
    //        end = objs.length - cur;
    //    if( end == 0 )
    //        return [];
    //    res = objs[ cur .. cur + end ];
    //    cur += end;
    //    return res;
    //}

    T[] getAll()
    { 
        T[] ret;
        auto res = db.call( format("select * from %s;", table_name) );
        foreach( r; res )
            ret ~= parse( r );
        return ret;
    }

    //void first(){ cur = 0; }

}

class StarTable : ObjTable!StarInfo
{
private:
    AvailStarsTable availstarstable;
    float[] getSpectrFromString( string str )
    {
        import std.array;
        import std.algorithm;
        auto vals = str.split(",");
        return array( map!( a => to!float(a) )(vals) );
    }
protected:

    override @property string table_name()const { return "stars"; }

    override StarInfo parse( string s )
    {
        auto sp = s.split("|");    
        import core.exception;

        try
        {
            auto id = to!size_t(sp[cols["id"]]);
            auto name = sp[cols["properName"]];
            auto x = to!float(sp[cols["x"]]);
            auto y = to!float(sp[cols["y"]]);
            auto z = to!float(sp[cols["z"]]);
            auto distance = to!float(sp[cols["distance"]]);
            auto bright = 0.0;
            auto absBright = to!float(sp[cols["absMag"]]); 
            auto r = to!float(sp[cols["r"]]);
            auto g = to!float(sp[cols["g"]]);
            auto b = to!float(sp[cols["b"]]);
            auto spectr = getSpectrFromString( sp[cols["full_spectr"]] );

            auto pos = vec3( x, y, z );
            if( pos.e )
                pos = pos.e;
            
            auto avstars = availstarstable.getAvailableJumpStars( id );

            return StarInfo( id, name, pos, 
                        distance, bright, absBright,
                        col3( r,g,b ), spectr.idup, avstars.idup );
        }
        catch( RangeError e )
            throw new DBException( format("No such column: At line %d", e.line) );
    }
public 
    this( SQLiteApi _db, AvailStarsTable _astarstable )
    {
        availstarstable = _astarstable;
        super(_db); 
    }
}

class PlanetTable : ObjTable!PlanetInfo
{
private:
    SatelliteTable sat;
protected:

    override @property string table_name() const { return "planets"; }

    override PlanetInfo parse( string s )
    {
        auto sp = s.split("|");    
        import core.exception;
        
        try
        {
            auto id = to!size_t(sp[cols["id"]]);
            auto pid = to!size_t(sp[cols["pid"]]);
            auto name = sp[cols["name"]];
            auto x = to!float(sp[cols["x"]]);
            auto y = to!float(sp[cols["y"]]);
            auto z = to!float(sp[cols["z"]]);
            auto distance = to!float(sp[cols["dist"]]);
            auto size = to!float( sp[cols["size"]] );
            auto desc = sp[cols["desc"]];
            auto img = sp[cols["img"]];
            auto disk = sp[cols["disk"]];
            auto near_radius = to!float( sp[cols["near_radius"]] );
            auto far_radius = to!float( sp[cols["far_radius"]] );
            auto disk_alpha = to!float( sp[cols["disk_alpha"]] );
            auto disk_beta = to!float( sp[cols["disk_beta"]] );

            auto sats = sat.getByParent(id);

            auto rinfo = RingInfo( disk, [ near_radius, far_radius ], 
                                    vec2( disk_alpha, disk_beta ) );
            return PlanetInfo( id, pid, name, vec3( x, y, z ).e, 
                        distance, size, desc, img, rinfo, sats.idup );
        }
        catch( RangeError e )
            throw new DBException( format( "No such column: At line %d.", e.line ));
    }

public: 
    this( SQLiteApi _db, SatelliteTable sat )
    { 
        this.sat = sat;
        super(_db); 
    }

    PlanetInfo[] getByParent( size_t pid )
    {
        auto raw_res = db.call( format( "select * from %s where pid like %d;", table_name, pid ) );
        
        PlanetInfo[] res;
        foreach( r; raw_res )
            res ~= parse(r);
        return res;
    }
}

private class SatelliteTable : ObjTable!SatelliteInfo
{
protected:

    override @property string table_name() const { return "satellites"; }

    override SatelliteInfo parse( string s ) const
    {
        auto sp = s.split("|");    
        import core.exception;
        
        try
        {
            auto id = to!size_t(sp[cols["id"]]);
            auto pid = to!size_t(sp[cols["pid"]]);
            auto name = sp[cols["name"]];
            auto x = to!float(sp[cols["x"]]);
            auto y = to!float(sp[cols["y"]]);
            auto z = to!float(sp[cols["z"]]);
            auto distance = to!float(sp[cols["dist"]]);
            auto size = to!float( sp[cols["size"]] );
            auto desc = sp[cols["desc"]];
            auto img = sp[cols["img"]];
            auto mesh = sp[cols["mesh"]];

            return SatelliteInfo( id, pid, name, vec3( x, y, z ).e, 
                        distance, size, desc, img, mesh );
        }
        catch( RangeError e )
            throw new DBException( format( "No such column: At line %d.", e.line ));
    }

public: 
    this( SQLiteApi _db ){ super(_db); }//TODO checkout the right constructor

    SatelliteInfo[] getByParent( size_t pid )
    {
        auto raw_res = db.call( format( "select * from %s where pid like %d;", table_name, pid ) );
        
        SatelliteInfo[] res;
        foreach( r; raw_res )
            res ~= parse(r);
        return res;
    }
}

class ConstellationTable : ObjTable!ConstellationInfo
{
protected:

    override @property string table_name() const { return "constellations"; }

    override ConstellationInfo parse( string s ) const
    {
        auto sp = s.split("|");    
        import core.exception;
        
        try
        {
            auto id = to!size_t(sp[cols["id"]]);
            auto name = sp[cols["name"]];
            auto desc = sp[cols["description"]];

            return ConstellationInfo( id, name, desc );
        }
        catch( RangeError e )
            throw new DBException( format( "No such column: At line %d.", e.line ));
    }

public: 
    this( SQLiteApi _db ){ super(_db); }//TODO checkout the right constructor
}

class ConstellationPartTable : ObjTable!ConstellationPartInfo
{
protected:

    override @property string table_name() const { return "constellation_parts"; }

    override ConstellationPartInfo parse( string s ) const
    {
        auto sp = s.split("|");    
        import core.exception;
        
        try
        {
            auto id = to!size_t(sp[cols["id"]]);
            auto cid = to!size_t(sp[cols["constellation_id"]]);
            auto p1str = sp[cols["point1"]];
            auto p2str = sp[cols["point2"]];
            
            auto spstr = p1str.split(",");
            auto x = to!float( spstr[0] );
            auto y = to!float( spstr[1] );
            auto z = to!float( spstr[2] );
            auto p1 = vec3( x, y, z );

            spstr = p2str.split(",");
            x = to!float( spstr[0] );
            y = to!float( spstr[1] );
            z = to!float( spstr[2] );
            auto p2 = vec3( x, y, z );

            return ConstellationPartInfo( id, cid, p1, p2 );
        }
        catch( RangeError e )
            throw new DBException( format( "No such column: At line %d.", e.line ));
    }

public: 
    this( SQLiteApi _db ){ super(_db); }//TODO checkout the right constructor

    ConstellationPartInfo[] getByParent( size_t pid )
    {
        auto res = db.call( format("select * from %s where constellation_id like %d;", table_name, pid) );
        ConstellationPartInfo[] ret;
        foreach( r; res )
        {
            ret ~= parse(r);
        }
        return ret;
    }
}

private class AvailStarsTable : ObjTable!AvailStar
{
protected:

    override @property string table_name() const { return "avail_stars"; }

    override AvailStar parse( string s ) const
    {
        auto sp = s.split("|");    
        import core.exception;
        
        try
        {
            auto src_star = to!size_t(sp[cols["src_id"]]);
            auto dst_star = to!size_t(sp[cols["des_id"]]);

            return AvailStar( src_star, dst_star );
        }
        catch( RangeError e )
            throw new DBException( format( "No such column: At line %d.", e.line ));
    }

public: 
    this( SQLiteApi _db ){ super(_db); }//TODO checkout the right constructor

    size_t[] getAvailableJumpStars( size_t id )
    {
        size_t[] ret;
        auto res = db.call( format("select * from %s where src_id like %d;", table_name, id) );
        foreach( r; res )
        {
            auto astar = parse( r );
            ret ~= astar.dst_star;
        }
        return ret;
    }
}

class SpaceDB : ExternalMemoryManager
{
    mixin EMM;
private:
    SQLiteApi api;

    AvailStarsTable astable;
    SatelliteTable satellite;
public:
    StarTable stars;
    PlanetTable planets;
    ConstellationTable cons;
    ConstellationPartTable cons_part;

    this( string fname )
    {
        api = newEMM!SQLiteApi( fname );
        astable = newEMM!AvailStarsTable( api );
        stars = newEMM!StarTable( api, astable );
        satellite = newEMM!SatelliteTable( api );
        planets = newEMM!PlanetTable( api, satellite );
        cons = newEMM!ConstellationTable( api );
        cons_part = newEMM!ConstellationPartTable( api );
    }
}
