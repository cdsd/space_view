module calc.separator;

import des.math.linear;
import des.util.logsys;

import std.algorithm;
import std.string;

class Separator
{
private:

    size_t slevel;
    dvec3[] arr;

    static dvec3[] generate( size_t level )
    {
        static struct Triangle
        {
            dvec3[3] vert;

            @property dvec3 center() const 
            { return (vert[0] + vert[1] + vert[2]) / 3.0; }

            @property dvec3 c_sp() const { return center.e; }

            Triangle spherise() const
            { return Triangle( [ vert[0].e, vert[1].e, vert[2].e ] ); }

            Triangle[4] subdive() const
            {
                dvec3[3] sub;
                sub[0] = (vert[0] + vert[1]) / 2.0;
                sub[1] = (vert[1] + vert[2]) / 2.0;
                sub[2] = (vert[2] + vert[0]) / 2.0;

                return [
                        Triangle( [ sub[0], sub[1], sub[2] ] ),
                        Triangle( [ vert[0], sub[0], sub[2] ] ),
                        Triangle( [ sub[0], vert[1], sub[1] ] ),
                        Triangle( [ sub[2], sub[1], vert[2] ] )
                    ];
            }
        }

        static struct Node
        {
            Triangle mesh;
            Node[] ch;
        }

        // IcoSphere
        immutable root_vert = [
            dvec3(  0.0000000000000000e+00,  0.0000000000000000e+00, -1.0000000000000000e+00 ),
            dvec3(  7.2359997034072876e-01, -5.2572000026702881e-01, -4.4721499085426331e-01 ),
            dvec3( -2.7638500928878784e-01, -8.5063999891281128e-01, -4.4721499085426331e-01 ),
            dvec3( -8.9442497491836548e-01,  0.0000000000000000e+00, -4.4721499085426331e-01 ),
            dvec3( -2.7638500928878784e-01,  8.5063999891281128e-01, -4.4721499085426331e-01 ),
            dvec3(  7.2359997034072876e-01,  5.2572000026702881e-01, -4.4721499085426331e-01 ),
            dvec3(  2.7638500928878784e-01, -8.5063999891281128e-01,  4.4721499085426331e-01 ),
            dvec3( -7.2359997034072876e-01, -5.2572000026702881e-01,  4.4721499085426331e-01 ),
            dvec3( -7.2359997034072876e-01,  5.2572000026702881e-01,  4.4721499085426331e-01 ),
            dvec3(  2.7638500928878784e-01,  8.5063999891281128e-01,  4.4721499085426331e-01 ),
            dvec3(  8.9442497491836548e-01,  0.0000000000000000e+00,  4.4721499085426331e-01 ),
            dvec3(  0.0000000000000000e+00,  0.0000000000000000e+00,  1.0000000000000000e+00 )
        ];

        auto root = [
            Node( Triangle( [ root_vert[0],  root_vert[1],  root_vert[2]  ] ) ),
            Node( Triangle( [ root_vert[1],  root_vert[0],  root_vert[5]  ] ) ),
            Node( Triangle( [ root_vert[0],  root_vert[2],  root_vert[3]  ] ) ),
            Node( Triangle( [ root_vert[0],  root_vert[3],  root_vert[4]  ] ) ),
            Node( Triangle( [ root_vert[0],  root_vert[4],  root_vert[5]  ] ) ),
            Node( Triangle( [ root_vert[1],  root_vert[5],  root_vert[10] ] ) ),
            Node( Triangle( [ root_vert[2],  root_vert[1],  root_vert[6]  ] ) ),
            Node( Triangle( [ root_vert[3],  root_vert[2],  root_vert[7]  ] ) ),
            Node( Triangle( [ root_vert[4],  root_vert[3],  root_vert[8]  ] ) ),
            Node( Triangle( [ root_vert[5],  root_vert[4],  root_vert[9]  ] ) ),
            Node( Triangle( [ root_vert[1],  root_vert[10], root_vert[6]  ] ) ),
            Node( Triangle( [ root_vert[2],  root_vert[6],  root_vert[7]  ] ) ),
            Node( Triangle( [ root_vert[3],  root_vert[7],  root_vert[8]  ] ) ),
            Node( Triangle( [ root_vert[4],  root_vert[8],  root_vert[9]  ] ) ),
            Node( Triangle( [ root_vert[5],  root_vert[9],  root_vert[10] ] ) ),
            Node( Triangle( [ root_vert[6],  root_vert[10], root_vert[11] ] ) ),
            Node( Triangle( [ root_vert[7],  root_vert[6],  root_vert[11] ] ) ),
            Node( Triangle( [ root_vert[8],  root_vert[7],  root_vert[11] ] ) ),
            Node( Triangle( [ root_vert[9],  root_vert[8],  root_vert[11] ] ) ),
            Node( Triangle( [ root_vert[10], root_vert[9],  root_vert[11] ] ) )
        ];

        void subdive( Node[] list, size_t lvl )
        {
            if( lvl == 0 ) return;

            foreach( ref n; list )
            {
                foreach( t; n.mesh.subdive() )
                    n.ch ~= Node( t.spherise() );
                subdive( n.ch, lvl - 1 );
            }
        }

        subdive( root, level );

        dvec3[] flat( Node[] list )
        {
            if( list.length == 0 ) return [];
            dvec3[] ret;
            foreach( n; list )
                ret ~= ( n.ch.length ? [] : [ n.mesh.c_sp ] ) ~ flat( n.ch );
            return ret;
        }

        return flat( root );
    }

    size_t tfind( in dvec3 pnt, ptrdiff_t cnt, ptrdiff_t cur, ptrdiff_t start ) const
    {
        if( cur < 1 ) return start;

        auto step = 4 ^^ cur;

        double min = double.max;
        ptrdiff_t no = -1;

        foreach( i; 0 .. cnt )
        {
            auto k = start+i*step;
            auto dst = ( pnt - arr[k] ).len2;
            if( min > dst )
            {
                min = dst;
                no = k;
            }
        }

        return tfind( pnt, 4, cur-1, no );
    }

    bool nl_ready = false;
    size_t[12][] nearlist;

    void find_near()
    {
        static struct HLP(T)
        {
            size_t id;
            T val;
        }

        HLP!(dvec3)[] hlp;
        hlp.length = arr.length;

        foreach( i, v; arr ) hlp[i] = HLP!dvec3(i,v);

        logger.info( "separator find sub places" );
        foreach( i, ref nl; nearlist )
        {
            /+ TRUE BUT SLOW
            auto p = arr[i];
            bool cmp( in HLP a, in HLP b ){ return (p-a.pos).len2 < (p-b.pos).len2; }
            partialSort!cmp( hlp, 12 );
            auto fn( in HLP a ) { return a.id; }
            auto res = map!fn( hlp[0 .. 12] );
            size_t k = 0;
            foreach( v; res ) nl[k++] = v;
            +/

            auto p = arr[i];
            HLP!(float)[] res;
            foreach( j, v; hlp )
            {
                HLP!float buf;
                buf.id = j;
                buf.val = (arr[i] - hlp[j].val).len2;
                if( buf.val > 0.16 ) continue;

                if( res.length < 12 ) res ~= buf;
                else
                {
                    auto bres = res ~ buf;
                    bool cmp( in HLP!float a, in HLP!float b ){ return a.val < b.val; }

                    sort!cmp( bres );
                    res = bres[0..12];
                }
            }
            foreach( k, r; res ) nl[k] = r.id;
        }
        logger.info( "complite" );
        nl_ready = true;
    }

public:
    this( size_t Level, bool req_near=false )
    {
        arr = generate( Level ); 
        slevel = Level;

        nearlist.length = arr.length;

        //version(release)
        if(req_near) find_near();
    }

    size_t find(T,string AS)( in Vector!(3,T,AS) pnt ) const
        if( is(T:double) )
    { 
        if( !pnt )
            throw new Exception( format( "find point has nan: [%f %f %f]", 
                        pnt.x, pnt.y, pnt.z ) );
        return tfind( dvec3(pnt).e, 20, slevel, 0 ); 
    }

    size_t[] near( size_t ind ) const
    { return [ind] ~ ( nl_ready ? nearlist[ind] : [] ); }

    @property size_t level() const { return slevel; }
    @property size_t count() const { return arr.length; }
    @property dvec3[] array() const { return arr.dup; }

    dvec3 opIndex(size_t i) const { return arr[i]; }
}
