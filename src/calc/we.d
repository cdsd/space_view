module calc.we;

import des.flow;
import des.util.helpers;
import des.util.stdext.algorithm;
import des.math.linear;

import utils.yamlext;
import utils.flowext;
import utils.appext;
import utils.types;

import calc.starparse;
import calc.separator;

alias StarInfo[] StarInfoBlock;

class CalcWE : WorkElement, EventProcessor
{
private:
    ApplicationStateHandler astate_handler;
    SpaceDB database;
    Separator separator;

    StarInfo[] stars;

    StarInfoBlock[size_t] blocks;

    bool need_stars = false;

    StarInfo calcNewStarInfo( in StarInfo star, in StarInfo current )
    {
        if( current.id == star.id )
            return star;
        import std.math;
        StarInfo res_star = star;
        auto star_pos = ( res_star.np * res_star.dist ) - ( current.np * current.dist );
        res_star.np = star_pos.e;
        res_star.dist = star_pos.len;

        res_star.bright = res_star.absBright + 5 * log10( res_star.dist / 10.0 );//Какая то формула из интернета
        res_star.bright = -res_star.bright + 7;
        return res_star;
    }

    void sortStarsToBlocks( in StarInfo current )
    {
        foreach( i, star; stars )
        {
            if( current.id == star.id )
                continue;
            auto new_star_info = calcNewStarInfo( star, current );
            if( new_star_info.bright <= 1 || new_star_info.bright > 20 ) continue;
            if( new_star_info.np.len2 == 0 || !new_star_info.np )
                continue;
            auto id = separator.find( new_star_info.np );
            if( id in blocks )
                blocks[id] ~= new_star_info;
            else
                blocks[id] = [new_star_info];
        }
    }

    void calcAndSendInfo( vec3 ray )
    {
        if( ray.len == 0 || !ray )
            return;
        auto eray = ray.e;
        auto id = separator.find( eray );
        auto neib_block_ids = separator.near( id );
        auto min = float.max;
        StarInfo res_star;
        foreach( i; neib_block_ids )
        {
            if( i !in blocks )
                continue;
            foreach( star; blocks[i] )
            {
                float len = ( star.np - ray ).len;
                if( len < min )
                {
                    min = len;
                    res_star = star;
                }
            }
        }
        pushEvent( Event( EvCode.STAR_INFO_BY_RAY, res_star ) );
    }

    void prepareAppStateHandler()
    {
        astate_handler.starByIdCallback = (size_t id)
        { return database.stars[id]; };
    }
public:
    this( in YAMLNode set, FlowStateHandler state_handler, ApplicationStateHandler astate_handler )
    {
        this.astate_handler = astate_handler;
        prepareAppStateHandler();
        auto database_file_name = tryYAML!string( set, "database", "hygxyz.db" );
        database = registerChildEMM( astate_handler.getDatabase( database_file_name ) );
        separator = new Separator( 4 );//TODO may be setting
    }

    override void process()
    {
        if( !need_stars )
            return;
        if( astate_handler.isFirstRun )
        {
            astate_handler.gotFirstRun();
            stars = database.stars.getAll();
        }
        sortStarsToBlocks( astate_handler.currentStar );
        StarInfo[] res_stars;
        foreach( b; blocks )//TODO change on std
            foreach( s; b )
                res_stars ~= s;
        pushEvent( Event( EvCode.STARS, res_stars ) );
        need_stars = false;
    }

    override EventProcessor[] getEventProcessors() { return [this]; }//TODO Нужно ли это вообще?

    override void processEvent( in Event ev )
    {
        switch( ev.code )
        {
            case EvCode.GET_STARS:
                need_stars = true;
                blocks.destroy();
                break;
            case EvCode.GET_STAR_INFO_BY_RAY:
                calcAndSendInfo( ev.as!(vec3) );
                break;
            default:
                break;
        }
    }
}
