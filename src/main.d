import utils.flowext;
import utils.appext;
import utils.yamlext;

import draw.we;
import calc.we;

import des.util.helpers;

WorkElement createWE(T)( in YAMLNode set, 
        FlowStateHandler fstate_handler, 
        ApplicationStateHandler astate_handler )
    if( is( T : WorkElement ) )
{ return new T( set, fstate_handler, astate_handler ); }

void main()
{
    auto app_data_path = appPath( "..", "data" );
    auto config_file_name = "config/cfg.yaml";
    auto shader_folder_name = "shader";
    auto texture_folder_name = "texture";
    auto database_folder_name = "database";
    auto font_folder_name = "font";

    auto app = new ApplicationStateHandler( app_data_path, 
                                            config_file_name,
                                            shader_folder_name,
                                            texture_folder_name,
                                            database_folder_name,
                                            font_folder_name );

    auto sys = new FlowStateHandler( app );

    auto we_funcs = [ &( createWE!DrawWE ), &( createWE!CalcWE ) ];//TODO нахуя это блять!!!!
    auto config_roots = [ "draw", "calc" ];
    sys.prepareThreads( we_funcs, config_roots );
    sys.addListener( "draw", "calc" );
    sys.addListener( "calc", "draw" );

    sys.run();
}



